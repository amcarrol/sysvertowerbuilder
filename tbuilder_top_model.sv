////////////////////////////////
//file: tbuilder_top.sv
//Author: Anthony Carroll (acarrol4@uoregon.edu)
//purpose: System Verilog version of the gFEX firmware tbuilder_top module, made for testing
////////////////////////////////
//inclusions
`include "gt_builder.svh"

module tbuilder_top_model #(
    //parameters
    parameter FPGA_NMBR = 0,
    parameter NFBRS = ABfibers,
    parameter ENABLE_PUC = 0
) 
(
    //inputs
    input logic i_clk_f,
    input logic i_rst,

    //static signals used to set operating mode
    input logic i_do_lconv,
    input logic [15:0] i_back_thr, //15 is FPGA_CALGT_DW-1: check file inclusions eventually

    //IP Bus Interface 
    input logic i_ipb_clk, //input:ip bus clock(70 MHz)
    input logic i_IP_mode, //input:  1 allows slow control operations
    input logic[23:0] i_IPbus_adr, //input: STD_LOGIC[23 downto 0]
    input logic[15:0] i_IPbus_din, //input: STD_LOGIC[15 downto 0]
    input logic i_IP_wre, //input:  write enable for IP mode
    output logic o_IPbus_rd, // output: output to IP bus interface from calibration memories-

    //input data
    input logic [0:NFBRS-1][31:0] i_lar_data, //input:  ALBL_INP(0 to NFBRS-1);
    input logic i_lar_tuser_sof, //input:  STD_LOGIC
    input logic i_lar_tvalid, //input:  STD_LOGIC
    input logic i_lar_tlast_eof, //input:  STD_LOGIC
    output logic o_lar_tready, //output: STD_LOGIC
    //input LAR auxiliary signals - not processed by this entity, just delayed to be aligned with o_gt_* data
    input logic i_lar_tbcr, //input:  STD_LOGIC
    input logic i_lar_tmetafrm, //input:  STD_LOGIC
    input logic i_lar_tcrcerr, //input:  STD_LOGIC

    //output data - new axi-s interface
    output logic o_gt_tuser_sof, //output: STD_LOGIC
    output logic o_gt_tvalid, //output: STD_LOGIC
    output logic o_gt_tlast_eof, //output: STD_LOGIC
    output logic[15:0] o_gt_tdata_e0_5[0:31], //output: GTBLD_OUTP
    output logic[15:0] o_gt_tdata_e6_11[0:31], //output: GTBLD_OUTP
    output logic [31:0] o_gt_ov_e0_5, //output: STD_LOGIC_VECTOR(31 downto 0)
    output logic [31:0] o_gt_ov_e6_11, //output: STD_LOGIC_VECTOR(31 downto 0)
    //output LAR auxiliary signals - aligned with o_gt_* data
    output logic o_gt_tbcr, //output: STD_LOGIC
    output logic o_gt_tmetafrm, //output: STD_LOGIC
    output logic o_gt_tcrcerr, //output: STD_LOGIC

    //BCID aligned with o_gt_* bus
    output logic [6:0] o_gt_aligned_bcid, //output: STD_LOGIC_VECTOR(6 downto 0)
    output logic [6:0] o_bcid_tdata, //output: STD_LOGIC_VECTOR(6 downto 0)
    output logic o_bcid_tuser_sof, //output: STD_LOGIC
    output logic o_bcid_tvalid, //output: STD_LOGIC
    output logic o_bcid_tlast_eof, //output: STD_LOGIC
    output logic o_puc_dav, //output: STD_LOGIC
    output logic [15:0] o_puc_data //output: STD_LOGIC_VECTOR(FPGA_ADDER_DW-1 downto 0)
);
    //functional code

    //To start - set all outputs to 0
    assign o_IPbus_rd = 0;
    assign o_lar_tready = 0;
    assign o_gt_tuser_sof = 0;
    assign o_gt_tvalid = 0;
    assign o_gt_tlast_eof = 0;
    assign o_gt_tdata_e0_5 = '{default:'0};
    assign o_gt_tdata_e6_11 = '{default:'0};
    assign o_gt_ov_e0_5 = '{default:'0};
    assign o_gt_ov_e6_11 = '{default:'0};
    assign o_gt_tbcr = 0;
    assign o_gt_tmetafrm = 0;
    assign o_gt_tcrcerr = 0;
    assign o_gt_aligned_bcid = '{default:'0};
    assign o_bcid_tdata = '{default:'0};
    assign o_bcid_tuser_sof = 0;
    assign o_bcid_tvalid = 0;
    assign o_bcid_tlast_eof = 0;
    assign o_puc_dav = 0;
    assign o_puc_data = '{default:'0};
    


endmodule : tbuilder_top_model
