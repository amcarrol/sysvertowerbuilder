//////////////////////////////////////////////////////////
//File: tbuilder_mapper_model.sv
//Author: Anthony Carroll (acarrol4@uoregon.edu)
//Purpose: Create a System Verilog model for the gFEX Tower Builder mapper for use in SystemVerilog Simulation
//////////////////////////////////////////////////////////
module tbuilder_mapper_model(
    //inputs
    input logic clk_f, //fabric clock, not sure about clocks... will have to ask Kirk
    input logic i_dav, //not sure what this is yet
    input int Xfib[MAXfibers][W280], //array of fibers
    input int FPGANUM //FPGA identifier, A=0, B=1, C=2

    output logic o_dav, //output corresponding to i_dav
    output logic[6:0] o_bcid_tdata, //bcid output logic
    output logic o_bcid_tuser_sof, //output
    output logic o_bcid_tvalid, //output
    output logic o_bcid_tlast_eof, //output
    output int etower_data[324], //etower data
    output int htower_data[324], //htower data
    output int xetower_data[32], //extended region etower
    output int xhtower_data[32], //extended region htower
    output int ohtower_data[32] //overlap region
);

    //initialize output arrays to 0
    for(int i=0,i<324,i++) begin
        etower_data[i]=0;
        htower_data[i]=0;
    end

    for(int i=0,i<32,i++) begin
        xetower_data[i]=0;
        xhtower_data[i]=0;
        ohtower_data[i]=0;
    end

    //pFPGA A
    if(FPGANUM==0)begin
        
        for(i=0,i<ABfibers,i++)begin
            for(j=0,j<MAXEfields,j++)begin //j is data chunk, use MAXEfields(16), maybe MAXfields(20)- follow up later
                //BCID_DRV
                if(i==0)begin
                    if(AMPD_DTYP_ARR[AMPD_NFI[i]][j]=="0b1010") begin
                        process(clk_f) begin
                            if(rising_edge(clk_f)) begin
                                if(i_dav == 1) begin
                                    // dont understand the longer logic array yet o_bcid_tdata <= ??
                                    o_bcid_tuser_sof <= "1";
                                    o_bcid_tvalid <= "1";
                                    o_bcid_tlast_eof <= "1"; 
                                end else begin
                                    o_bcid_tuser_sof <= "0";
                                    o_bcid_tvalid <= "0";
                                    o_bcid_tlast_eof <= "0";                                 
                                end
                            end           
                        end    
                    end
                end
                //ETD
                if(AMPD_DTYP_ARR[AMPD_NFI[i]][j]=="0b0000") begin
                    process(clk_f) begin
                        if(rising_edge(clk_f)) begin
                            if(i_dav == 1) begin
                                etower_data[AMPD_GTRN_ARR[i][j]][11 downto 0] <= Xfib[i][AMPD_DSTRT_ARR[AMPD_NFI[i]][j] downto AMPD_DSTRT_ARR[AMPD_NFI[i]][j]-11]
                            end
                        end           
                    end    
                end

                //HTD_TREX
                if(AMPD_DTYP_ARR[AMPD_NFI[i]][j]=="0b0001") begin
                    process(clk_f) begin
                        if(rising_edge(clk_f)) begin
                            if(i_dav == 1) begin
                                htower_data[AMPD_GTRN_ARR[i][j]][11 downto 0] <= Xfib[i][AMPD_DSTRT_ARR[AMPD_NFI[i]][j] downto AMPD_DSTRT_ARR[AMPD_NFI[i]][j]-11];
                                //ht_is_trex?
                            end
                        end       
                    end    
                end

                //HTD_HEC
                if(AMPD_DTYP_ARR[AMPD_NFI[i]][j]=="0b1011") begin
                    process(clk_f) begin
                        if(rising_edge(clk_f)) begin    
                            if(i_dav == 1) begin
                                htower_data[AMPD_GTRN_ARR[i][j]][11 downto 0] <= Xfib[i][AMPD_DSTRT_ARR[AMPD_NFI[i]][j] downto AMPD_DSTRT_ARR[AMPD_NFI[i]][j]-11];    
                            end
                        end    
                    end    
                end

                //XETD
                if(AMPD_DTYP_ARR[AMPD_NFI[i]][j]=="0b0010") begin
                    process(clk_f) begin
                        if(rising_edge(clk_f)) begin
                            if(i_dav == 1) begin
                                xetower_data[AMPD_GTRN_ARR[i][j]][11 downto 0] <= Xfib[i][AMPD_DSTRT_ARR[AMPD_NFI[i]][j] downto AMPD_DSTRT_ARR[AMPD_NFI[i]][j]-11];
                            end
                        end
                    end    
                end

                //XHTD
                if(AMPD_DTYP_ARR[AMPD_NFI[i]][j]=="0b0011") begin
                    process(clk_f) begin
                        if(rising_edge(clk_f)) begin
                            if(i_dav == 1) begin
                                xhtower_data[AMPD_GTRN_ARR[i][j]][11 downto 0] <= Xfib[i][AMPD_DSTRT_ARR[AMPD_NFI[i]][j] downto AMPD_DSTRT_ARR[AMPD_NFI[i]][j]-11];
                            end
                        end
                    end   
                end

                //OHTD
                if(AMPD_DTYP_ARR[AMPD_NFI[i]][j]=="0b0110") begin
                    process(clk_f) begin
                        if(rising_edge(clk_f)) begin
                            if(i_dav == 1) begin
                                ohtower_data[AMPD_GTRN_ARR[i][j]][11 downto 0] <= Xfib[i][AMPD_DSTRT_ARR[AMPD_NFI[i]][j] downto AMPD_DSTRT_ARR[AMPD_NFI[i]][j]-11];
                            end
                        end
                    end   

                end

                //SAT_GEN_8_A
                for(int k=0,k<8,k++)begin
                    //SAT_LOW
                    if(AMPD_DTYP_ARR[AMPD_NFI[i]][j]=="0b1000")begin
                        //SAT_LOW_EMB
                        if(AMPD_DTYP_ARR[AMPD_NFI[i]][k*2]=="0b0000")begin
                            process(clk_f) begin
                                if(rising_edge(clk_f)) begin
                                    if(i_dav == 1) begin
                                        etower_data[AMPD_GTRN_ARR[i][k*2]][12] <= Xfib[i][AMPD_DSTRT_ARR[AMPD_NFI[i]][j]-7+k];
                                    end   
                                end
                            end
                        end
                        //SAT_LOW_HEC
                        if(AMPD_DTYP_ARR[AMPD_NFI[i]][k]=="0b1011")begin
                            process(clk_f) begin
                                if(rising_edge(clk_f)) begin
                                    if(i_dav == 1) begin
                                        htower_data[AMPD_GTRN_ARR[i][k]][12] <= Xfib[i][AMPD_DSTRT_ARR[AMPD_NFI[i]][j]-7+k];
                                    end   
                                end
                            end                          
                        end
                        //SAT_LOW_EMEC
                        if(AMPD_DTYP_ARR[AMPD_NFI[i]][k]=="0b0010")begin
                            process(clk_f) begin
                                if(rising_edge(clk_f)) begin
                                    if(i_dav == 1) begin
                                        xetower_data[AMPD_GTRN_ARR[i][k]][12] <= Xfib[i][AMPD_DSTRT_ARR[AMPD_NFI[i]][j]-7+k];
                                    end   
                                end
                            end
                        end
                        //SAT_LOW_EXT_HEC
                        if(AMPD_DTYP_ARR[AMPD_NFI[i]][k]=="0b0011")begin
                            process(clk_f) begin
                                if(rising_edge(clk_f)) begin
                                    if(i_dav == 1) begin
                                        xhtower_data[AMPD_GTRN_ARR[i][k]][12] <= Xfib[i][AMPD_DSTRT_ARR[AMPD_NFI[i]][j]-7+k];
                                    end   
                                end
                            end
                        end
                        //SAT_LOW_OVLAP_HEC 
                        if(AMPD_DTYP_ARR[AMPD_NFI[i]][k]=="0b0110")begin
                            process(clk_f) begin
                                if(rising_edge(clk_f)) begin
                                    if(i_dav == 1) begin
                                        ohtower_data[AMPD_GTRN_ARR[i][k]][12] <= Xfib[i][AMPD_DSTRT_ARR[AMPD_NFI[i]][j]-7+k];
                                    end   
                                end
                            end
                        end                                               
                    end
                    //SAT_HIGH
                    if(AMPD_DTYP_ARR[AMPD_NFI[i]][j]=="0b1001")begin
                        //SAT_HIGH_EMEC
                        if(AMPD_DTYP_ARR[AMPD_NFI[i]][k+8]=="0b0010")begin
                            process(clk_f) begin
                                if(rising_edge(clk_f)) begin
                                    if(i_dav == 1) begin
                                        xetower_data[AMPD_GTRN_ARR[i][k+8]][12] <= Xfib[i][AMPD_DSTRT_ARR[AMPD_NFI[i]][j]-7+k];
                                    end   
                                end
                            end
                        end
                        //SAT_HIGH_EXT_HEC
                        if(AMPD_DTYP_ARR[AMPD_NFI[i]][k+8]=="0b0011")begin
                            //stuff
                        end
                        //SAT_HIGH_OVLAP_HEC
                        if(AMPD_DTYP_ARR[AMPD_NFI[i]][k+8]=="0b0110")begin
                            //stuff
                        end                        
                    end
                end
            end
        end

    end
    //pFPGA B
    if(FPGANUM==1)begin

        for(i=0,i<ABfibers,i++)begin
            for(j=0,j<MAXEfields,j++)begin //j is data chunk, use MAXEfields(16), maybe MAXfields(20)- follow up later
                //BCID_DRV
                if(i==0)begin
                    if(BMPD_DTYP_ARR[BMPD_NFI[i]][j]=="0b1010") begin
                        process(clk_f) begin
                            if(i_dav = 1) begin
                                //stuff
                            end else begin
                                //more stuff
                            end       
                        end    
                    end
                end
                //ETD
                if(BMPD_DTYP_ARR[BMPD_NFI[i]][j]=="0b0000") begin
                    process(clk_f) begin
                        if(i_dav == 1) begin
                            //stuff
                        end else begin
                            //more stuff
                        end       
                    end    
                end

                //HTD_TREX
                if(BMPD_DTYP_ARR[BMPD_NFI[i]][j]=="0b0001") begin
                    process(clk_f) begin
                        if(i_dav == 1) begin
                            //stuff
                        end else begin
                            //more stuff
                        end       
                    end    
                end

                //HTD_HEC
                if(BMPD_DTYP_ARR[BMPD_NFI[i]][j]=="0b1011") begin
                    process(clk_f) begin
                        if(i_dav == 1) begin
                            //stuff
                        end else begin
                            //more stuff
                        end       
                    end    
                end

                //XETD
                if(BMPD_DTYP_ARR[BMPD_NFI[i]][j]=="0b0010") begin
                    process(clk_f) begin
                        if(i_dav == 1) begin
                            //stuff
                        end else begin
                            //more stuff
                        end       
                    end    
                end

                //XHTD
                if(BMPD_DTYP_ARR[BMPD_NFI[i]][j]=="0b0011") begin
                    process(clk_f) begin
                        if(i_dav == 1) begin
                            //stuff
                        end else begin
                            //more stuff
                        end       
                    end    
                end

                //OHTD
                if(BMPD_DTYP_ARR[BMPD_NFI[i]][j]=="0b0110") begin
                    process(clk_f) begin
                        if(i_dav == 1) begin
                            //stuff
                        end else begin
                            //more stuff
                        end       
                    end    

                end

                //SAT_GEN_8_B
                for(int k=0,k<8,k++)begin
                    //SAT_LOW
                    if(BMPD_DTYP_ARR[BMPD_NFI[i]][j]=="0b1000")begin
                        //SAT_LOW_EMB
                        if(BMPD_DTYP_ARR[BMPD_NFI[i]][k*2]=="0b0000")begin
                            //stuff
                        end
                        //SAT_LOW_HEC
                        if(BMPD_DTYP_ARR[BMPD_NFI[i]][k]=="0b1011")begin
                            //stuff
                        end
                        //SAT_LOW_EMEC
                        if(BMPD_DTYP_ARR[BMPD_NFI[i]][k]=="0b0010")begin
                            //stuff
                        end
                        //SAT_LOW_EXT_HEC
                        if(BMPD_DTYP_ARR[BMPD_NFI[i]][k]=="0b0011")begin
                            //stuff
                        end
                        //SAT_LOW_OVLAP_HEC 
                        if(BMPD_DTYP_ARR[BMPD_NFI[i]][k]=="0b0110")begin
                            //stuff
                        end                                               
                    end
                    //SAT_HIGH
                    if(BMPD_DTYP_ARR[BMPD_NFI[i]][j]=="0b1001")begin
                        //SAT_HIGH_EMEC
                        if(BMPD_DTYP_ARR[BMPD_NFI[i]][k+8]=="0b0010")begin
                            //stuff
                        end
                        //SAT_HIGH_EXT_HEC
                        if(BMPD_DTYP_ARR[BMPD_NFI[i]][k+8]=="0b0011")begin
                            //stuff
                        end
                        //SAT_HIGH_OVLAP_HEC
                        if(BMPD_DTYP_ARR[BMPD_NFI[i]][k+8]=="0b0110")begin
                            //stuff
                        end                        
                    end
                end
            end
        end        
    end
    //pFPGA C    
    if(FPGANUM==2)begin
        for(int i=0, i<Cfibers,i++)begin
            for(int j=0, j<MAXEfields, j++)begin
                //BCID_DRV
                if(i==0)begin
                    if(CMPD_DTYP_ARR[CMPD_NFI[i][j]]=="0b1010")begin
                        //stuff
                    end
                end
                //XE_TWR_GEN_0
                if(CMPD_DTYP_ARR[CMPD_NFI[i]][j]=="0b0010")begin
                    //stuff
                end
                //XH_TWR_GEN_0
                if(CMPD_DTYP_ARR[CMPD_NFI[i]][j]=="0b0011")begin
                    //stuff
                end
                //k loop - saturation stuff
                for(int k=0, k<8, k++)begin
                    //LOW_SAT_0
                    if(CMPD_DTYP_ARR[CMPD_NFI[i]][j]=="0b1000")begin
                        //XE_LOW_0
                        if(CMPD_DTYP_ARR[CMPD_NFI[i]][k]=="0b0010")begin
                            //stuff
                        end
                        //XH_LOW_0
                        if(CMPD_DTYP_ARR[CMPD_NFI[i]][k]=="0b0011")begin
                            //stuff
                        end
                    end
                    //HIGH_SAT_0
                    if(CMPD_DTYP_ARR[CMPD_NFI[i]][j]=="0b1001")begin
                        //XH_HIGH_0
                        if(CMPD_DTYP_ARR[CMPD_NFI[i]][k+8]=="0b0011")begin
                            //stuff
                        end
                        //XE_HIGH_0
                        if(CMPD_DTYP_ARR[CMPD_NFI[i]][k+8]=="0b0010")begin
                            //stuff
                        end
                    end    
                end
            end
        end
    end
endmodule: tbuilder_mapper_model

/*
Some Relevant Information:

Data comes in on 80 fibers in seven clocks.   

Alltogether there are 224 bits (7*32) per fiber.   

Individual fiber words (appropriated delayed) are albl_in(I) 

These are packed integer a single word data_in(i) 

For EMB, etc this will have 4 bits position + 12 energy.  Position is currently ignored.   

For all others there are 16*12 = 192 bits of energy information.   
*/
/*

KEEPING THE UNDOMLE HERE FOR NOW, WILL PROBABLY MOVE LATER - AC

// make up weights to use for deconstructiuon towers 
float weights[16] = { 1.0, 0.25, 0.5, 0.12, 0, 0, 0.05, 0, 0, 0, 0, 0.25, 0, 0, 0, 0 };

module undoMLE(
    input int *datumPtr
);

    int din = *datumPtr;
    int dout=0;
    
    int FPGA_CONVLIN_TH1 = 5; 
    int FPGA_CONVLIN_TH2 = 749;
    int FPGA_CONVLIN_TH3 = 1773;
    int FPGA_CONVLIN_TH4 = 2541;
    int FPGA_CONVLIN_TH5 = 4029;

    int FPGA_CONVLIN_OF0 = -5072;
    int FPGA_CONVLIN_OF1 = -2012;
    int FPGA_CONVLIN_OF2 = -1262;
    int FPGA_CONVLIN_OF3 = -3036;
    int FPGA_CONVLIN_OF4 = -8120;
    int FPGA_CONVLIN_OF5 = -4118720;

    int oth0 = 0;
    int oth1 = 0;
    int oth2 = 0;
    int oth3 = 0;
    int oth4 = 0;
    int oth5 = 0;
  
    int r1shv = 0;
    int r2shv = 0;
    int r3shv = 0;
    int r4shv = 0;
    int r5shv = 0;
    int r6shv = 0;
    int trxv = 0;

    int r1conv = 0;
    int r2conv = 0;
    int r3conv = 0;
    int r4conv = 0;
    int r5conv = 0;
    int r6conv = 0;
    int r3offs = 0;

    r1shv = ((din & 0x0000007F) << 9 )  & 0x0000FE00 ;
    r2shv = ((din & 0x00000FFF) << 1 )  & 0x00001FFE ;
    r3shv = (din &  0x00000FFF) ;
    r4shv = ((din & 0x00000FFF) << 1 )  & 0x00001FFE ;
    r5shv = ((din & 0x00000FFF) << 2 )  & 0x00003FFC ;
    r6shv = ((din & 0x00000FFF) << 10 ) & 0x003FFC00 ;

    r1conv =  r1shv + FPGA_CONVLIN_OF0;
    r2conv =  r2shv + FPGA_CONVLIN_OF1;
    r3conv =  r3shv + FPGA_CONVLIN_OF2;
    r4conv =  r4shv + FPGA_CONVLIN_OF3;
    r5conv =  r5shv + FPGA_CONVLIN_OF4;
    r6conv =  r6shv + FPGA_CONVLIN_OF5;

    if( din > 0 ) begin
        oth0 = 1;
    end
    else begin
        oth0 = 0; 
    end
    if ( din > FPGA_CONVLIN_TH1 )begin
        oth1 = 1;
    end
    else begin
        oth1 = 0; 
    end
    if ( din > FPGA_CONVLIN_TH2 )begin
        oth2 = 1;
    end
    else begin
        oth2 = 0; 
    end
    if ( din > FPGA_CONVLIN_TH3 )begin
        oth3 = 1;
    end
    else begin
        oth3 = 0; 
    end
    if ( din > FPGA_CONVLIN_TH4 )begin
        oth4 = 1;
    end 
    else begin
        oth4 = 0; 
    end
    if ( din > FPGA_CONVLIN_TH5 )begin
        oth5 = 1;
    end
    else begin
        oth5 = 0; 
    end

// divide by 2 to 50 MeV LSB

    if( (! oth0) & (! oth1 ) & (! oth2 ) & (! oth3 ) &  (! oth4 ) & (! oth5 )  ) begin
        dout = 0;
    end 
    else if( ( oth0) & (! oth1 ) & (! oth2 ) & (! oth3 ) &  (! oth4 ) & (! oth5 )  ) begin
        dout =  r1conv/2;
    end
    else if( ( oth0) & (  oth1 ) & (! oth2 ) & (! oth3 ) &  (! oth4 ) & (! oth5 )  ) begin
        dout = r2conv/2;
    end 
    else if( ( oth0) & (  oth1 ) & ( oth2 ) & (! oth3 ) &  (! oth4 ) & (! oth5 )  ) begin
        dout = r3conv/2;
    end  
    else if( ( oth0) & (  oth1 ) & (  oth2 ) & ( oth3 ) &  (! oth4 ) & (! oth5 )  ) begin
        dout = r4conv/2;
    end  
    else if( ( oth0) & (  oth1 ) & (  oth2 ) & ( oth3 ) &  (  oth4 ) & (! oth5 )  ) begin
        dout = r5conv/2;
    end  
    else if( ( oth0) & (  oth1 ) & (  oth2 ) & ( oth3 ) &  (  oth4 ) & (  oth5 )  ) begin
        dout = r6conv/2;
    end 
    else begin
        dout = 0; 
    end
     
    *datumPtr = dout; 


endmodule: undoMLE
*/
